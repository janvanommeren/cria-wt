/*jslint browser: true*/
/*global expect*/
/*global it*/
/*global describe*/
/*global spyOn*/
/*global stockQuotes*/
/*global getQuotes*/
/*global console*/
/*global jasmine*/
/*global beforeEach*/
/*global afterEach*/
describe('drawQuotes', function () {
    'use strict';
    beforeEach(function () {
        jasmine.Ajax.install();
    });
    afterEach(function () {
        jasmine.Ajax.uninstall();
    });
    it('should build and remove a table', function () {
        var draw, data = {
            "query": {
                "results": {
                    "quote": [{
                        "Name": "test",
                        "Symbol": "test",
                        "PercentChange": "+1%",
                        "Change": 10,
                        "Open": 20
                    }]
                }
            }
        };
        draw = new stockQuotes.drawQuotes();
        draw.buildPage(data);
        expect(document.getElementById('quotesTable').id).toEqual('quotesTable');
        draw.removeTable();
        expect(document.getElementById('quotesTable')).toEqual(null); 
    });
    it('should change the displayed data', function () {
        var draw, data = {
            "query": {
                "results": {
                    "quote": [{
                        "Name": "test",
                        "Symbol": "test",
                        "PercentChange": "+1%",
                        "Change": 10,
                        "Open": 20
                    }]
                }
            }
        };
        draw = new stockQuotes.drawQuotes();
        data = draw.modifyData(data);
        expect(data.query.results.quote[0].Open).not.toEqual(20);
        expect(data.query.results.quote[0].Change).not.toEqual(10);
        expect(data.query.results.quote[0].PercentChange).not.toEqual('+1%');
    });
});