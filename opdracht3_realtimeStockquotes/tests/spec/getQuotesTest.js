/*jslint browser: true*/
/*global expect*/
/*global it*/
/*global describe*/
/*global spyOn*/
/*global stockQuotes*/
/*global getQuotes*/
/*global console*/
/*global jasmine*/
/*global beforeEach*/
/*global afterEach*/
describe('getQuotes', function () {
    'use strict';
    beforeEach(function () {
        jasmine.Ajax.install();
    });
    afterEach(function () {
        jasmine.Ajax.uninstall();
    });
    it('should call the getFromYahoo with callback when constructed', function () {
        var getQuotes, doneFn = jasmine.createSpy("success");
        jasmine.Ajax.stubRequest('https://query.yahooapis.com/v1/public/yql?q=select%20Change%2C%20PercentChange%2C%20Symbol%2C%20Name%2C%20Open%20from%20yahoo.finance.quotes%20where%20symbol%20in%0A(%22BCS%22%2C%22STT%22%2C%22JPM%22%2C%22LGEN.L%22%2C%22UBS%22%2C%22DB%22%2C%22BEN%22%2C%22CS%22%2C%22BK%22%2C%22KN.PA%22%2C%22GS%22%2C%22LM%22%2C%22MS%22%2C%22MTU%22%2C%22NTRS%22%2C%22GLE.PA%22%2C%22BAC%22%2C%22AV%22%2C%22SDR.L%22%2C%22DODGX%22%2C%22SLF%22%2C%22SL.L%22%2C%22NMR%22%2C%22ING%22%2C%22BNP.PA%22)&format=json&diagnostics=false&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys').andReturn({
            "responseText": 1
        });
        getQuotes = new stockQuotes.getQuotes(doneFn);
        expect(doneFn).toHaveBeenCalled();
    });
});