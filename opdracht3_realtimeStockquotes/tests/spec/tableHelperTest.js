/*jslint browser: true*/
/*global expect*/
/*global it*/
/*global describe*/
/*global tableHelper*/
describe('tableHelper', function () {
    it('Should append a column to a row', function () {
        var row = document.createElement('tr');
        row = tableHelper.appendColumn(row, 'test');
        expect(row.innerHTML).toEqual('<td>test</td>');
    });
});