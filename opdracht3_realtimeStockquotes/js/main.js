/*jslint browser: true*/
var stockQuotes = {};
/**
 * Function for starting the application.           
 */
stockQuotes.init = function () {
    'use strict';
    var drawQuotes = new stockQuotes.drawQuotes();
    drawQuotes.initUpdatePage();
};

window.onload = stockQuotes.init;