/*global stockQuotes*/
/*jslint browser: true*/
var tableHelper = {
    /**
     * Append a column on a given row with a given value
     * @param  row      The row where the column needs to be appended to
     * @param  value    The value to be inserted into the new column.            
     */
    appendColumn: function (row, value) {
        'use strict';
        var column = document.createElement('td');
        column.innerHTML = value;
        row.appendChild(column);
        return row;
    }

};