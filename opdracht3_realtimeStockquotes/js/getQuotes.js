/*global stockQuotes*/
/*jslint browser: true*/
/**
 * constructs the object and then executes the getFromYahoo() function
 * @param  callback     The callback to be executed and to pass the data to as parameter 
 */
stockQuotes.getQuotes = function (callback) {
    'use strict';
    if (typeof callback === 'function') {
        this.getFromYahoo(callback);
    }
};
/**
 * get data from the yahoo api
 * @param  callback     The callback to be executed and to pass the data to as parameter             
 */
stockQuotes.getQuotes.prototype.getFromYahoo = function (callback) {
    'use strict';
    var url, req, data;
    url = 'https://query.yahooapis.com/v1/public/yql?q=select%20Change%2C%20PercentChange%2C%20Symbol%2C%20Name%2C%20Open%20from%20yahoo.finance.quotes%20where%20symbol%20in%0A(%22BCS%22%2C%22STT%22%2C%22JPM%22%2C%22LGEN.L%22%2C%22UBS%22%2C%22DB%22%2C%22BEN%22%2C%22CS%22%2C%22BK%22%2C%22KN.PA%22%2C%22GS%22%2C%22LM%22%2C%22MS%22%2C%22MTU%22%2C%22NTRS%22%2C%22GLE.PA%22%2C%22BAC%22%2C%22AV%22%2C%22SDR.L%22%2C%22DODGX%22%2C%22SLF%22%2C%22SL.L%22%2C%22NMR%22%2C%22ING%22%2C%22BNP.PA%22)&format=json&diagnostics=false&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys';
    req = new XMLHttpRequest();
    req.onreadystatechange = function () {
        if (req.readyState === 4 && req.status === 200) {
            data = JSON.parse(req.responseText);
            if (typeof callback === 'function') {
                callback(data);
            }
        }
    };
    req.open("GET", url, true);
    req.send();
};