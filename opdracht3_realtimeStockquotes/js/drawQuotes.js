/*global stockQuotes*/
/*global tableHelper*/
/*jslint browser: true, devel: true*/
/**
 * constructs the object and then executes the getQuotes constructor
 */
stockQuotes.drawQuotes = function () {
    'use strict';
    stockQuotes.getQuotes.call(this, this.buildPage);
};
/**
 * copy the prototype from getQuotes to use inheritance
 */
stockQuotes.drawQuotes.prototype = Object.create(stockQuotes.getQuotes.prototype);
/**
 * Draws the table on the page.
 * @param  data     The data to be inserted into the table.
 */
stockQuotes.drawQuotes.prototype.buildPage = function (data) {
    'use strict';
    var body, quotes, rowCount, table, row, highest, lowest;
    table = document.createElement('table');
    table.id = 'quotesTable';
    quotes = data.query.results.quote;
    if(typeof this !== 'undefined') {
        highest = this.getHighest(quotes);
        lowest = this.getLowest(quotes);
    } else {
        highest = -1;
        lowest = -2;
    }
    for (rowCount = 0; rowCount < quotes.length; rowCount += 1) {
        row = document.createElement('tr');
        row = tableHelper.appendColumn(row, quotes[rowCount].Symbol);
        row = tableHelper.appendColumn(row, quotes[rowCount].Name);
        row = tableHelper.appendColumn(row, quotes[rowCount].Open);
        row = tableHelper.appendColumn(row, quotes[rowCount].Change);
        row = tableHelper.appendColumn(row, quotes[rowCount].PercentChange);
        if(rowCount === highest) {
            row = tableHelper.appendColumn(row, '<canvas id="highest" height="15" width="15"></canvas>');
        } else if(rowCount === lowest) {
            row = tableHelper.appendColumn(row, '<canvas id="lowest" height="15" width="15"></canvas>');
        } else {
            row = tableHelper.appendColumn(row, '');
        }
        table.appendChild(row);
    }
    body = document.getElementsByTagName('body')[0];
    body.appendChild(table);
};
/**
 * Finds the lowest stock.
 * @param  data     The data from the yahoo api call. 
 * @return index    The position of the lowest stock.
 */
stockQuotes.drawQuotes.prototype.getLowest = function (data) {
    'use strict';
    var lowest, index, i;
    lowest = 99;
    index = 0;
    for(i = 0; i < data.length; i += 1) {
        if(data[i].Change < lowest) {
            lowest = data[i].Change;
            index = i;
        }
    }
    return index;
};
/**
 * Finds the highest stock.
 * @param  data     The data from the yahoo api call. 
 * @return index    The position of the highest stock.
 */
stockQuotes.drawQuotes.prototype.getHighest = function (data) {
    'use strict';
    var highest, index, i;
    highest = 0;
    index = 0;
    for(i = 0; i < data.length; i += 1) {
        if(data[i].Change > highest) {
            highest = data[i].Change;
            index = i;
        }
    }
    return index;
};
/**
 * Remove the table from the page.
 */
stockQuotes.drawQuotes.prototype.removeTable = function () {
    'use strict';
    if (document.getElementById('quotesTable') !== null) {
        var table = document.getElementById('quotesTable');
        table.parentElement.removeChild(table);
    }
};
/**
 * Modifies the data random from the yahoo api.
 * @param  data     The data from the yahoo api call to be randomized. 
 * @return data     The randomized data.
 */
stockQuotes.drawQuotes.prototype.modifyData = function (data) {
    'use strict';
    var resultCount, random;
    for (resultCount = 0; resultCount < data.query.results.quote.length; resultCount += 1) {
        random = Math.floor(Math.random() * 100) / 100;
        if (Math.floor(Math.random() * 2) + 1 === 1) {
            data.query.results.quote[resultCount].PercentChange = '+' + random + '%';
            data.query.results.quote[resultCount].Open += random;
            data.query.results.quote[resultCount].Change += random;
        } else {
            data.query.results.quote[resultCount].PercentChange = '-' + random + '%';
            data.query.results.quote[resultCount].Open -= random;
            data.query.results.quote[resultCount].Change -= random;
        }
    }
    return data;
};
/**
 * Get the canvas elements and draw the arrows.
 */
stockQuotes.drawQuotes.prototype.drawArrows = function () {
    'use strict';
    var lowest, highest, context;
    lowest = document.getElementById('lowest');
    context = lowest.getContext('2d');
    this.drawArrow(context, 7.5, 0, 7.5, 15, 8, '#ff0000');

    highest = document.getElementById('highest');
    context = highest.getContext('2d');
    this.drawArrow(context, 7.5, 15, 7.5, 0, 8, '#000000');
};
/**
 * Draw a arrow.
 * @param  context      The context from the canvas element, see drawArrows(). 
 * @param  fromx        The starting point on the x axis for drawing the start line.
 * @param  fromy        The starting point on the y axis for drawing the start line.
 * @param  tox          The ending x position for the arrow head.
 * @param  toy          The ending y position for the arrow head.
 * @param  headLength   The length in pixels of the arrow head.
 * @param  color        The color of the arrow.
 */
stockQuotes.drawQuotes.prototype.drawArrow = function (context, fromx, fromy, tox, toy, headLength, color) {
    'use strict';
    var angle = Math.atan2(toy-fromy,tox-fromx);
    context.moveTo(fromx, fromy);
    context.lineTo(tox, toy);
    context.lineTo(tox-headLength*Math.cos(angle-Math.PI/6),toy-headLength*Math.sin(angle-Math.PI/6));
    context.moveTo(tox, toy);
    context.lineTo(tox-headLength*Math.cos(angle+Math.PI/6),toy-headLength*Math.sin(angle+Math.PI/6));
    context.strokeStyle = color;
    context.stroke();
};
/**
 * set a interval to update the page every two seconds.            
 */
stockQuotes.drawQuotes.prototype.initUpdatePage = function () {
    'use strict';
    var result, self = this;
    setInterval(function () {
        self.getFromYahoo(function (data) {
            result = self.modifyData(data);
            self.removeTable();
            self.buildPage(result);
            self.drawArrows();
        });
    }, 2000);
};