# README

Author:   Jan van Ommeren
Version:  0.9
Since:    30-10-2014
 
## Inleiding
De applicatie begint bij het bestand main.js, dit bestand is het aanroeppunt voor de stockQuotes. In dit bestand wordt een object gedefineerd genaamd stockQuotes, dit object functioneert als namespace zodat er geen variabelen of functies met de zelfde naam bestaan. Vervolgens wordt de start functie gedefinceerd, deze functie is het startpunt van de applicatie.

## Bestanden
Hieronder zullen de gebruikte objecten en events beschreven worden per onderdeel.

### main.js
Alle functies in het bestand main.js hebben een verandwoordelijkheid met betrekking tot het starten van de applicatie.

Function: `stockQuotes.init();`
Description: Deze functie moet gebruikt worden om stockquotes te starten.
Return: Void

### getQuotes.js
getQuotes.js is bedoeld voor het ophalen van de gegvevens vanaf yahoo, het is ook mogelijk om tijdens het aanmaken van het object al een callback mee te geven, in dat geval zal de functie `getFromYahoo(callback)` meteen worden uitgevoerd.

Function: `StockQuotes.getQuotes.getFromYahoo(callback)`
Description: Deze functie haalt de stock gegevens op van yahoo en voert vervolgens de callback aan die als parameter wordt meegegven met als parameter de opgehaalde data.
Param callback: De callback die wordt aangeroepen na het ophalen van de data.
Return: Void

### drawQuotes.js
draqQuotes wordt gebruikt voor het laten zien van de data die met getQuotes is opgehaald. Dit wordt gedaan door met de volgende regel: `stockQuotes.drawQuotes.prototype = Object.create(stockQuotes.getQuotes.prototype);` over te erven van getQuotes zodat er nu toegang is tot de `getFromYahoo(callback)` functie die vervolgens in de constructer wordt aangeroepen.

Function: `stockQuotes.drawQuotes.buildPage(data)`
Description: Deze functie bouwt een tabel op basis van de data die wordt meegegven als parameter, dit zullen altijd de gegevens uit de functie `getFromYahoo(callback)` moeten zijn.
Param data: De data die is opgehaald van yahoo.
Return: Void.

Function: `stockQuotes.drawQuotes.removeTable()`
Description: Deze functie verwijderd de tabel die is aangemaakt door de functie `buildPage(data)`.
Return: Void.

Function: `stockQuotes.drawQuotes.modifyData(data)`
Description: Deze functie veranderd de quotes data die in de parameter wordt meegegeven met random gegenereerde data.
Param data: De data die veranderd dient te worden.
Return: De aangepaste data.

Function: `stockQuotes.drawQuotes.initUpdatePage`
Description: Deze functie wordt gebruikt in main.js om aan te geven dat nadat de tabel gebouwd is hij om een x aantal seconden te updaten.
Return: Void.

### tableHelper.js
De tableHelper is een helper klasse die overal in de applicatie beschikbaar is.

Function: `tableHelper.appendColumn(row, value)`
Description: Deze functie voegt een `<td>` toe aan een `<tr>` element met daarin de waarde die meegegeven wordt als value.
Param row: Het `<tr>` element waaraan een kolom wordt toegevoegd.
Param value: De waarde die in de toe te voegen kolom komt.
Return: Het nieuwe `<tr>` element.