﻿/*jslint devel:true, nomen:true, unparam: true*/
/*global module, __filename, require*/
var validationHelper = require(__dirname + '/../helpers/ValidationHelper.js')();
/**
 * exports all the functions and the controller to the app
 * @param  {object} mongoose    Mongoose provides a straight-forward, schema-based solution to modeling your application data and includes built-in type casting
 * @param  {Object} test        A test of the application
 * @return {Object}             Returns a list of functions
 */
module.exports = function (mongoose, Test) {
    'use strict';
    var request = require('request');
    return {
        /**
        * Execute a test
        * @param  {Object}   req  the received request
        * @param  {object}   res  the sended responce
        * @return {Object}   returns the result of the executed test
        */
        doRequest: function (req, res) {
            if (!validationHelper.isValidTest(req.body)) {
                return res.send('invalid test');
            }
            var name = req.body.name;
            var options = {
                uri: req.body.url,
                headers: req.body.headers,
                method: req.body.method,
                body: req.body.data
            };
            if (req.body.duration !== null && req.body.duration > 0) {
                options.timeout = req.body.duration;
            }
            if (options.headers == '""') {
                options.headers = '';
            }
            request(options, function (error, response, body) {
                if (typeof response === 'undefined') {
                    response = {};
                    response.statusCode = 501;
                }
                return res.status(response.statusCode).send({
                    testName: name,
                    error: error,
                    response: response,
                    body: body
                });
            });
        },
        /**
        * Gets a list of tests
        * @param  {Object}   req  the received request
        * @param  {object}   res  the sended responce
        * @param  {Function} next the next function in this controller find
        * @return {Object}      returns a list of tests
        */
        list: function (req, res, next) {
            if (!validationHelper.isDocumentId(req.user._doc._id)) {
                res.send('invalid id');
            }
            var conditions = { userId: req.user._doc._id };
            var fields = {};
            var options = {};
            
            Test.find(conditions, fields, options)
            .exec(function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'list',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            });
        },
        /**
         * Gets a scenario by its id
         * @param  {Object}   req  the received request
         * @param  {object}   res  the sended responce
         * @param  {Function} next the next function in this controller create
         * @return {Object}        returns a test
         */
        find: function (req, res) {
            if (!validationHelper.isDocumentId(req.params.id)) {
                return res.send('id is invalid');
            }
            var conditions = { _id: req.params.id };
            var fields = {};
            var options = {};
            
            Test.find(conditions, fields, options)
            .exec(function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'find',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc[0],
                    err: err
                };
                return res.send(retObj);
            });
        },
        /**
         * Creates a new test
         * @param  {Object}   req  the received request
         * @param  {object}   res  the sended responce
         * @param  {Function} next the next function in this controller update
         * @return {Object}        returns a new test
         */
        create: function (req, res) {
            if (!validationHelper.isValidTest(req.body, false)) {
                return res.send('invalid test');
            }
            var doc = new Test(req.body);
            doc.userId = req.user._doc._id;
            doc.save(function (err) {
                var retObj = {
                    meta: {
                        action: 'create',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            });
        },
        /**
         * Updates a test
         * @param  {Object}   req  the received request
         * @param  {object}   res  the sended responce
         * @param  {Function} next the next function in this controller delete
         * @return {Object}        returns a test
         */
        update: function (req, res) {
            if (!validationHelper.isValidTest(req.body)) {
                res.send('test is invalid');
            }
            var conditions = { _id: req.params.id };
            var update = {
                name: req.body.name || '',
                headers: req.body.headers || '',
                data: req.body.data || '',
                url: req.body.url || '',
                method: req.body.method || '',
                duration: req.body.duration || '',
                timeout: req.body.timeout || '',
                expects: req.body.expects || ''
            };
            var options = { multi: false };
            var callback = function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'update',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            };
            Test.findByIdAndUpdate(conditions, update, options, callback);
        },
        /**
         * Deletes a test
         * @param  {Object}   req  the received request
         * @param  {object}   res  the sended responce
         * @return {Object}        returns a confirmation of the delete
         */
        delete: function (req, res) {
            if (!validationHelper.isDocumentId(req.params.id)) {
                return res.send('invalid id');
            }
            var conditions = { _id: req.params.id };
            var callback = function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'delete',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            };
            Test.remove(conditions, callback);
        }
    };
};