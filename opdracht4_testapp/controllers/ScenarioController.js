﻿/*jslint devel:true, nomen:true, unparam: true*/
/*global module, __filename*/
var validationHelper = require(__dirname + '/../helpers/ValidationHelper.js')();
/**
 * exports all the functions and the controller to the app
 * @param  {object} mongoose    Mongoose provides a straight-forward, schema-based solution to modeling your application data and includes built-in type casting
 * @param  {Object} scenario    A scenario of the application
 * @return {Object}             Returns a list of functions
 */
module.exports = function (mongoose, Scenario) {
    'use strict';
    /**
    * Gets a list of scenarios
    * @param  {Object}   req  the received request
    * @param  {object}   res  the sended responce
    * @param  {Function} next the next function in this controller find
    * @return {Object}      returns a list of scenarios
    */
    return {
        list: function (req, res, next) {
            if (!validationHelper.isDocumentId(req.user._doc._id)) {
                return res.send('id is invalid');
            }
            var conditions = { userId: req.user._doc._id };
            var fields = {};
            var options = {};
            
            Scenario.find(conditions, fields, options)
            .exec(function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'list',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            });
        },
        /**
         * Gets a scenario by its id
         * @param  {Object}   req  the received request
         * @param  {object}   res  the sended responce
         * @param  {Function} next the next function in this controller create
         * @return {Object}        returns a scenario
         */
        find: function (req, res) {
            if (!validationHelper.isDocumentId(req.params.id)) {
                return res.send('id is invalid');
            }
            var conditions = { _id: req.params.id };
            var fields = {};
            var options = {};
            
            Scenario.find(conditions, fields, options)
            .exec(function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'find',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc[0],
                    err: err
                };
                return res.send(retObj);
            });
        },
        /**
         * Creates a new scenario
         * @param  {Object}   req  the received request
         * @param  {object}   res  the sended responce
         * @param  {Function} next the next function in this controller update
         * @return {Object}        returns a new scenario
         */
        create: function (req, res) {
            var doc = new Scenario(req.body);
            if (!doc.userId) {
                doc.userId = req.user._doc._id;
            }
            doc.save(function (err) {
                var retObj = {
                    meta: {
                        action: 'create',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            });
        },
        /**
         * Updates a scenario
         * @param  {Object}   req  the received request
         * @param  {object}   res  the sended responce
         * @param  {Function} next the next function in this controller delete
         * @return {Object}        returns a scenario
         */
        update: function (req, res) {
            if (!validationHelper.isValidScenario(req.body)) {
                return res.send('scenario is invalid');
            }
            var conditions = { _id: req.params.id };
            var update = {
                name: req.body.name || '',
                tests: req.body.tests || ''
            };
            var options = { multi: false };
            var callback = function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'update',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            };
            Scenario.findByIdAndUpdate(conditions, update, options, callback);
        },
        /**
         * Deletes a scenario
         * @param  {Object}   req  the received request
         * @param  {object}   res  the sended responce
         * @return {Object}        returns a confirmation of the delete
         */
        delete: function (req, res) {
            if (!validationHelper.isDocumentId(req.params.id)) {
                return res.send('id is invalid');
            }
            var conditions = { _id: req.params.id };
            var callback = function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'delete',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            };
            Scenario.remove(conditions, callback);
        }
    };
};