﻿/*jslint devel:true, nomen:true, unparam: true*/
/*global module, require, __dirname*/
var validationHelper = require(__dirname + '/../helpers/ValidationHelper.js')();
/**
 * exports all the functions and the controller to the app
 * @param  {object} mongoose Mongoose provides a straight-forward, schema-based solution to modeling your application data and includes built-in type casting
 * @param  {Object} user     A user of the application
 * @return {Object}          Returns a list of functions
 */
module.exports = function (mongoose, User) {
    'use strict';
    /**
    * Gets a list of users
    * @param  {Object}   req  the received request
    * @param  {object}   res  the sended responce
    * @param  {Function} next the next function in this controller find
    * @return {Object}        returns a list of users
    */
    return {
        list: function (req, res, next) {
            var conditions = {};
            var fields = {};
            var options = {};
            
            User.find(conditions, fields, options)
            .exec(function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'list',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            });
        },
        /**
         * Gets a users by its id
         * @param  {Object}   req  the received request
         * @param  {object}   res  the sended responce
         * @param  {Function} next the next function in this controller create
         * @return {Object}        returns a user
         */
        find: function (req, res) {
            if (!validationHelper.isDocumentId(req.params.id)) {
                return res.send('invalid id');
            }
            var conditions = { _id: req.params.id };
            var fields = {};
            var options = {};
            
            User.find(conditions, fields, options)
            .exec(function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'find',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc[0],
                    err: err
                };
                return res.send(retObj);
            });
        },
        /**
         * Creates a new user
         * @param  {Object}   req  the received request
         * @param  {object}   res  the sended responce
         * @param  {Function} next the next function in this controller update
         * @return {Object}        returns a new user
         */
        create: function (req, res) {
            if (!validationHelper.isValidUser(req.body)) {
                return res.send('invalid user');
            }
            var doc = new User(req.body);
            doc.save(function (err) {
                var retObj = {
                    meta: {
                        action: 'create',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            });
        },
        /**
         * Updates a user
         * @param  {Object}   req  the received request
         * @param  {object}   res  the sended responce
         * @param  {Function} next the next function in this controller delete
         * @return {Object}        returns a user
         */
        update: function (req, res) {
            if (!validationHelper.isValidUser(req.body)||
                !validationHelper.isDoucmentId(req.params.id)) {
                return res.send('invalid user');
            }
            var conditions = { _id: req.params.id };
            var update = {
                name: req.body.name || '',
                email: req.body.email || '',
                isAdmin: req.body.isAdmin || ''
            };
            if (req.body.password) {
                update.password = req.body.password;
            }
            var options = { multi: false };
            var callback = function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'update',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            };
            User.findByIdAndUpdate(conditions, update, options, callback);
        },
        /**
         * Deletes a user
         * @param  {Object}   req  the received request
         * @param  {object}   res  the sended responce
         * @return {Object}        returns a confirmation of the delete
         */
        delete: function (req, res) {
            if (!validationHelper.isDocumentId(req.params.id)) {
                return res.send(req.params.id);
            }
            var conditions = { _id: req.params.id };
            var callback = function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'delete',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            };
            User.remove(conditions, callback);
        }
    };
};