db.createCollection('users');

db.users.insert({
    name: 'admin',
    email: 'janvanommeren@hotmail.com',
    password: '$2a$10$dKyrrcuxdlDQVnykiqbRCu.bICG.BHz8EYgoO7ekGaRtA7JYMxMme',
    isAdmin: true
});

db.users.insert({
    name: 'user',
    email: 'janvanommeren@gmail.com',
    password: '$2a$10$dKyrrcuxdlDQVnykiqbRCu.bICG.BHz8EYgoO7ekGaRtA7JYMxMme',
    isAdmin: false
});