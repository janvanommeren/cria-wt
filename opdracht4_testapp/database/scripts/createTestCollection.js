﻿db.createCollection('tests');

db.tests.insert({
    headers: { 'Content-Type': 'text' },
    url: 'http://test.com',
    method: 'GET',
    data: { username: 'test' },
    user_id: db.users.findOne({ name: 'admin' }, { _id: 1 })._id
});