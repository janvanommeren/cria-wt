//drop database and recreate it
db.dropDatabase();
db.getSiblingDB('TestApp');

//load creation scripts
load('createUserCollection.js');
load('createTestCollection.js');

//insert test data
