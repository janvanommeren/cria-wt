﻿describe('AuthService', function () {
    
    var AuthService;
    var httpBackend;

    beforeEach(function () {
        module('redRob');
        inject(function ($httpBackend, _AuthService_) {
            httpBackend = $httpBackend;
            AuthService = _AuthService_;
        }); 
    });
    
    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    }); 

    it('should login a user', function () {
        var user = {
            email: 'janvanommeren@gmail.com',
            password: 'test'
        };
        httpBackend.expectPOST('/login').respond('OK');
        var result = '';
        var prom = AuthService.login(user);
        prom.then(function (data) { 
            result = data;
        });
        httpBackend.flush();
        expect(result.data).toBe('OK');
    });

    it('should logout a user', function () {
        httpBackend.expectGET('/logout').respond('OK');
        var result = '';
        var prom = AuthService.logout();
        prom.then(function (data) { 
            result = data;
        });
        httpBackend.flush();
        expect(result.data).toBe('OK');
    });
});