﻿/*global testApp*/
/*jslint plusplus: true*/
/**
 * controller for showing and controlling the addScenario Form
 * @param  {Object} $scope          Scopes provide separation between the model and the view
 * @param  {Object} ScenarioFactory This is the resource service for CRUD operations on scenario objects
 * @param  {Object} TestFactory     This is the resource service for CRUD operations on tests objects 
 * @param  {Object} ngDialog        This is a third party plugin for showing a popup   
 * @return {void}                   no return value
 */
testApp.controller('AddScenarioController', ['$scope', 'ScenarioFactory', 'TestFactory', 'ngDialog',
    function ($scope, ScenarioFactory, TestFactory, ngDialog) {
        'use strict';
        /**
         * header text of the popup
         * @type {String}
         */
        $scope.headText = 'Scenario toevoegen';
        /**
         * text of the button save / add
         * @type {String}
         */
        $scope.btnSave = 'Toevoegen';
        /**
         * the current scenario, will be filled with data from the dialog
         * @type {String}
         */
        $scope.scenario = {};
        /**
         * init the values of the form with values obtained from the ngDialog
         * @return {void} no return value
         */
        initScenario();
        function initScenario() {
            $scope.tests = TestFactory.get(function () {
                $scope.selectedTest = $scope.tests.doc[0];
            });
            $scope.scenario.tests = [];
            if ($scope.ngDialogData && $scope.ngDialogData.scenario) {
                $scope.btnSave = 'Opslaan';
                $scope.headText = $scope.ngDialogData.scenario.name;
                $scope.scenario.name = $scope.ngDialogData.scenario.name;
                $scope.scenario.tests = $scope.ngDialogData.scenario.tests;
                $scope.scenario._id = $scope.ngDialogData.scenario._id;
            }
        }
        /**
         * add a test to the scenario
         * @return {void} no return value
         */
        $scope.addTest = function () {
            $scope.scenario.tests.push($scope.selectedTest);
        };
        /**
         * remove a test from the scenario
         * @return {void} no return value
         */
        $scope.removeTest = function (index) {
            $scope.scenario.tests.splice(index, 1);
        };
        /**
         * save the current scenario
         * @return {void} no return value
         */
        $scope.save = function (scenario) {
            if (scenario._id) {
                ScenarioFactory.update({ id: scenario._id }, scenario, function (res) { });
            } else {
                ScenarioFactory.save({}, scenario, function (res) { });
            }
            ngDialog.close();
        };
    }
]);