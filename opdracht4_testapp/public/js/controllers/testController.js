﻿/*global testApp*/
/**
 * controller for managing the tests
 * @param  {Object} $scope              Scopes provide separation between the model and the view
 * @param  {Object} $location           Angular object for redirecting users
 * @param  {Object} TestFactory         Factory for using REST for test objects
 * @param  {Object} ngDialog            Module for showing the dialog (popup)
 * @param  {Object} $timeout            Angular object for executing code after x miliseconds
 * @param  {Object} TestService         The service for executing tests on the backend
 * @return {void}   no return value
 */
testApp.controller('TestController', ['$scope', '$location', 'TestFactory', 'ngDialog', '$timeout', 'TestService',
    function ($scope, $location, TestFactory, ngDialog, $timeout, TestService) {
        'use strict';
        /**
         * array that holds all alerts displayed on the frontend
         * @type {Array}
         */
        $scope.alerts = [];
        /**
         * Get all tests
         * @return {void} no return value
         */
        $scope.loadTests = function () {
            $scope.tests = TestFactory.get({}, function () { },
                function (res) {
                if (res.status === 401) {
                    $location.path('/');
                }
            }
            );
        };
        $scope.loadTests();
        /**
         * Show the dialog (popup) for adding and editing a test
         * @param test      the test to be editted
         * @return {void}   no return value
         */
        $scope.showAdd = function (test) {
            ngDialog.open({
                template: 'partials/addTest.html',
                controller: 'AddTestController',
                data: { test: test }
            }).closePromise.then(function (data) {
                $timeout(function () {
                    $scope.loadTests();
                }, 500);
            });
        };
        /**
         * Copy a test
         * @param test      the test to be copied
         * @return {void}   no return value
         */
        $scope.copy = function (test) {
            delete test._id;
            TestFactory.save({}, test, function (res) {
                $scope.loadTests();
            });
        };
        /**
         * Delete a test
         * @param test      the test to be deleted
         * @return {void}   no return value
         */
        $scope.delete = function (test) {
            TestFactory.delete({ id: test._id }, function () {
                $scope.loadTests();
            });
        };
        /**
         * Execute a test
         * @param test      the test to be executed
         * @return {void}   no return value
         */
        $scope.doTest = function (test) {
            TestService.testRequest(test).then(function (data) {
                TestService.performExpect(test, data, function (res) { 
                    $scope.alerts.push(res);
                });
                $scope.alerts.push({ type: 'success', msg: data });
            }, function (data) {
                TestService.performExpect(test, data, function (res) {
                    $scope.alerts.push(res);
                });
                $scope.alerts.push({ type: 'danger', msg: data });
            });
        };
        /**
         * close alert by removing it from the array
         * @param index     the array[index] value of the alert to remove
         * @return {void}   no return value
         */
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
    }
]);