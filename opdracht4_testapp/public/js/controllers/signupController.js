﻿/*global testApp*/
/**
 * controller for signup page
 * @param  {Object} $scope          Scopes provide separation between the model and the view
 * @param  {Object} AuthService     The service for au 
 * @return {void}                   no return value
 */
testApp.controller('SignupController', ['$scope', 'AuthService', function ($scope, AuthService) {
  'use strict';
  /**
   * signup a user with the Authservice
   * @return {void} no return value
   */
  $scope.signup = function () {
      AuthService.signup({
          email: $scope.email,
          password: $scope.password
      });
  };
}]);