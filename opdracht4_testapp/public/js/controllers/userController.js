﻿/*global testApp*/
/**
 * controller for managing the users
 * @param  {Object} $scope              Scopes provide separation between the model and the view
 * @param  {Object} $location           Angular object for redirecting users
 * @param  {Object} UserFactory         Factory for using REST for user objects
 * @param  {Object} ngDialog            Module for showing the dialog (popup)
 * @param  {Object} $timeout            Angular object for executing code after x miliseconds
 * @return {void}   no return value
 */
testApp.controller('UserController', ['$scope', 'UserFactory', '$location', 'ngDialog', '$timeout',
    function ($scope, UserFactory, $location, ngDialog, $timeout) {
        'use strict';
        /**
         * Get all users
         * @return {void} no return value
         */
        $scope.loadUsers = function () {
            $scope.users = UserFactory.get();
        };
        $scope.loadUsers();
        /**
         * Show the dialog (popup) for adding a user
         * @param           the user to be editted
         * @return {void}   no return value
         */
        $scope.showAdd = function (user) {
            ngDialog.open({
                template: 'partials/addUser.html',
                controller: 'AddUserController',
                data: { user: user }
            }).closePromise.then(function (data) {
                $timeout(function () {
                    $scope.loadUsers();
                }, 200);
            });
        };
        /**
         * Delete a user
         * @param       The user to be deleted
         * @return      {void} no return value
         */
        $scope.delete = function (user) {
            UserFactory.delete({ id: user._id }, function () {
                $scope.loadUsers();
            });
        };
    }
]);