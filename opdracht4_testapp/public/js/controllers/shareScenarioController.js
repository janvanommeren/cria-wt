﻿/*global testApp*/
/*jslint plusplus: true*/
/**
 * controller for showing and controlling the addScenario Form
 * @param  {Object} $scope          Scopes provide separation between the model and the view
 * @param  {Object} ScenarioFactory This is the resource service for CRUD operations on scenario objects
 * @param  {Object} TestFactory     This is the resource service for CRUD operations on tests objects 
 * @param  {Object} ngDialog        This is a third party plugin for showing a popup   
 * @return {void}                   no return value
 */
testApp.controller('ShareScenarioController', ['$scope', 'ScenarioFactory', 'UserFactory', 'ngDialog',
    function ($scope, ScenarioFactory, UserFactory, ngDialog){
        /**
         * init the values of the form with values obtained from the ngDialog
         * @return {void} no return value
         */
        initScenario();
        function initScenario() {
            $scope.scenarios = ScenarioFactory.get(function () {
                $scope.selectedScenario = $scope.scenarios.doc[0];
            });
            $scope.users = UserFactory.get(function () {
                $scope.selectedUser = $scope.users.doc[0];
            });
        }
        /**
         * share the selected scenario with the selected user
         * @return {void} no return value
         */
        $scope.share = function () {
            var scenario = $scope.selectedScenario;
            scenario.shared = true;
            scenario.accepted = false;
            scenario.userId = $scope.selectedUser._id;
            delete scenario._id;
            console.log(scenario);
            ScenarioFactory.save({}, scenario, function (res) { });
            ngDialog.close();
        };
    }
]);