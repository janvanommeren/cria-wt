﻿/*global testApp*/
/**
 * controller for the home (login) page
 * @param  {Object} $scope          Scopes provide separation between the model and the view
 * @param  {Object} AuthService     The service for au 
 * @return {void}                   no return value
 */
testApp.controller('LoginController', ['$scope', 'AuthService', function ($scope, AuthService) {
    'use strict';
    /**
     * Login a user with the Authservice
     * @return {void} no return value
     */
    $scope.login = function () {
        AuthService.login({
            email: $scope.email,
            password: $scope.password
        });
    };
}]);