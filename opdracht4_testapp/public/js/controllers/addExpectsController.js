﻿/*global testApp*/
/*jslint plusplus: true*/
/**
 * controller for showing and controlling the addScenario Form
 * @param  {Object} $scope          Scopes provide separation between the model and the view
 * @param  {Object} ScenarioFactory This is the resource service for CRUD operations on scenario objects
 * @param  {Object} TestFactory     This is the resource service for CRUD operations on tests objects 
 * @param  {Object} ngDialog        This is a third party plugin for showing a popup   
 * @return {void}                   no return value
 */
testApp.controller('AddExpectsController', ['$scope', 'TestFactory', 'ngDialog',
    function ($scope, TestFactory, ngDialog) {
        'use strict';
        /**
         * init the values of the form with values obtained from the ngDialog
         * @return {void} no return value
         */
        initExpects();
        function initExpects() {
            $scope.test = {};
            if ($scope.ngDialogData && $scope.ngDialogData.test) {
                $scope.test.name = $scope.ngDialogData.test.name;
                $scope.test.headers = '' || JSON.stringify($scope.ngDialogData.test.headers);
                $scope.test.url = $scope.ngDialogData.test.url;
                $scope.test.method = $scope.ngDialogData.test.method;
                $scope.test.data = $scope.ngDialogData.test.data;
                $scope.test.duration = $scope.ngDialogData.test.duration;
                $scope.test.timeout = $scope.ngDialogData.test.timeout;
                $scope.test._id = $scope.ngDialogData.test._id;
                $scope.test.expects = $scope.ngDialogData.test.expects;
            }
            if (typeof $scope.test.expects === 'undefined') {
                $scope.test.expects = [];
            }
        }
        /**
         * add a expect to the test
         * @return {void} no return value
         */
        $scope.addExpect = function () {
            var expect;
            expect = $scope.currentExpect1 + ' ' + $scope.currentExpect2 + ' ' + $scope.currentExpect3
            $scope.test.expects.push(expect);
            $scope.currentExpect = '';
        };
        /**
         * remove a test from the scenario
         * @return {void} no return value
         */
        $scope.removeExpect = function (index) {
            $scope.test.expects.splice(index, 1);
        };
        /**
         * save the current test
         * @return {void} no return value
         */
        $scope.save = function (test) {
            TestFactory.update({ id: test._id }, test, function (res) { });
            ngDialog.close();
        };
    }
]);