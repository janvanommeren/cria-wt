﻿/*global testApp*/
/*jslint plusplus: true*/
/**
 * controller for showing and controlling the addScenario Form
 * @param  {Object} $scope          Scopes provide separation between the model and the view
 * @param  {Object} TestFactory     This is the resource service for CRUD operations on tests objects 
 * @param  {Object} ngDialog        This is a third party plugin for showing a popup   
 * @return {void}                   no return value
 */
testApp.controller('AddTestController', ['$scope', 'TestFactory', 'ngDialog', '$rootScope', 'ValidationService',
    function ($scope, TestFactory, ngDialog, $rootScope, ValidationService) {
        'use strict';
        /**
         * header text of the popup
         * @type {String}
         */
        $scope.headText = 'Test toevoegen';
        /**
         * text of the button save / add
         * @type {String}
         */
        $scope.btnSave = 'Toevoegen';
        /**
         * init the values of the form with values obtained from the ngDialog
         * @return {void} no return value
         */
        initTest();
        function initTest() {
            if ($scope.ngDialogData && $scope.ngDialogData.test) {
                $scope.btnSave = 'Opslaan';
                $scope.headText = $scope.ngDialogData.test.name;
                $scope.test = {};
                $scope.test.name = $scope.ngDialogData.test.name;
                $scope.test.headers = '' || JSON.stringify($scope.ngDialogData.test.headers);
                $scope.test.url = $scope.ngDialogData.test.url;
                $scope.test.method = $scope.ngDialogData.test.method;
                $scope.test.data = $scope.ngDialogData.test.data;
                $scope.test.duration = $scope.ngDialogData.test.duration;
                $scope.test.timeout = $scope.ngDialogData.test.timeout;
                $scope.test._id = $scope.ngDialogData.test._id;
            }
        }
        /**
         * Validates the url of the test
         * @return {void} no return value
         */
        $scope.validateUrl = function () { 
            if (!ValidationService.isUrl($scope.test.url)) {
                $scope.urlIsInvalid = true;
            } else {
                $scope.urlIsInvalid = false;
            }
        };
        /**
         * Validates the duration of the test
         * @return {void} no return value
         */
        $scope.validateDuration = function () {
            if (typeof $scope.test.duration === 'number' && $scope.test.duration % 1 === 0) {
                $scope.durationIsInvalid = true;
            } else {
                $scope.durationIsInvalid = false;
            }
        };
        /**
         * Validates the timeout of the test
         * @return {void} no return value
         */
        $scope.validateTimeout = function () {
            if (typeof $scope.test.timeout === 'number' && $scope.test.timeout % 1 === 0) {
                $scope.timeoutIsInvalid = true;
            } else {
                $scope.timeoutIsInvalid = false;
            }
        };
        /**
         * save the current test
         * @return {void} no return value
         */
        $scope.save = function (test) {
            if ($scope.urlIsInvalid === true) {
                return;
            }
            if (test._id) {
                if (ValidationService.isDocumentId(test._id)) {
                    TestFactory.update({ id: test._id }, test, function (res) { });
                } else {
                    alert("This test could not be updated, invalid ID.");
                }
            } else {
                TestFactory.save({}, test, function (res) { });
            }
            ngDialog.close();
        };
    }
]);