﻿/*global testApp*/
/**
 * maincontroller for handling outside of normal controller scope
 * @param  {Object} $scope          Scopes provide separation between the model and the view
 * @param  {Object} AuthService     The service for authenticating a user 
 * @param  {Object} $location       Angular variable for redirecting user to other pages
 * @param  {Object} $rootScope      The rootscope provided by angular to share values
 * @return {void}                   no return value
 */
testApp.controller('MainController', ['$scope', '$rootScope', '$location', 'AuthService',
    function ($scope, $rootScope, $location, AuthService) {
        'use strict';
        /**
         * value checked by view if user is a administrator
         * @type {Boolean}
         */
        $scope.isAdmin = false;
        /**
         * value checked by view if user is logged in
         * @type {Boolean}
         */
        $scope.isLoggedIn = false;
        /**
         * check if a user is logged in
         * @return {void} no return value
         */
        $scope.checkLogin = function () {
            if ($rootScope.currentUser) {
                $scope.isLoggedIn = true;
                $scope.isAdmin = $rootScope.currentUser.isAdmin;
            } else {
                $scope.isLoggedIn = false;
                $location.path('/');
            }
        };
        /**
         * logout a user
         * @return {void} no return value
         */
        $scope.logout = function () {
            AuthService.logout();
        };
        /**
         * set on view change event to check if user is logged in
         * @return {void} no return value
         */
        $scope.$on('$viewContentLoaded', function (event) {
            $scope.checkLogin();
        });
    }
]); 