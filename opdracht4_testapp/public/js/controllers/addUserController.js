﻿/*global testApp*/
/*jslint plusplus: true*/
/**
 * controller for showing and controlling the addUser Form
 * @param  {Object} $scope          Scopes provide separation between the model and the view
 * @param  {Object} UserFactory     This is the resource service for CRUD operations on scenario objects 
 * @param  {Object} ngDialog        This is a third party plugin for showing a popup   
 * @return {void}                   no return value
 */
testApp.controller('AddUserController', ['$scope', 'UserFactory', 'ngDialog', 'ValidationService',
    function ($scope, UserFactory, ngDialog, ValidationService) {
        'use strict';
        /**
         * header text of the popup
         * @type {String}
         */
        $scope.headText = 'Toevoegen';
        /**
         * init the values of the form with values obtained from the ngDialog
         * @return {void} no return value
         */
        initUser();
        function initUser() {
            if ($scope.ngDialogData && $scope.ngDialogData.user) {
                $scope.headText = 'Bewerken';
                $scope.user = {};
                $scope.user.email = $scope.ngDialogData.user.email;
                $scope.user.name = $scope.ngDialogData.user.name;
                $scope.user.isAdmin = $scope.ngDialogData.user.isAdmin;
                $scope.user._id = $scope.ngDialogData.user._id;
            }
        }
        /**
         * Validates the url of the test
         * @return {void} no return value
         */
        $scope.validateEmail = function () {
            if (!ValidationService.isEmail($scope.user.email)) {
                $scope.emailIsInvalid = true;
            } else {
                $scope.emailIsInvalid = false;
            }
        };
        /**
         * save the current scenario
         * @return {void} no return value
         */
        $scope.save = function (user) {
            if ($scope.emailIsInvalid === true) {
                return;
            }
            if (user._id) {
                if (ValidationService.isDocumentId(user._id)) {
                    UserFactory.update({ id: user._id }, user, function (res) { });
                } else {
                    alert("This user could not be updated, invalid ID.");
                }
            } else {
                UserFactory.save({}, user, function (res) { });
            }
            ngDialog.close();
        };
    }
]);