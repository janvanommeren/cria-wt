﻿/*global testApp*/
/**
 * controller for managing the tests
 * @param  {Object} $scope              Scopes provide separation between the model and the view
 * @param  {Object} TestFactory         Factory for using REST for test objects
 * @param  {Object} ngDialog            Module for showing the dialog (popup)
 * @param  {Object} $timeout            Angular object for executing code after x miliseconds
 * @return {void}   no return value
 */
testApp.controller('ExpectsController', ['$scope', '$location', 'TestFactory', 'ngDialog', '$timeout', 'TestService',
    function ($scope, $location, TestFactory, ngDialog, $timeout, TestService) {
        'use strict';
        /**
         * Get all tests
         * @return {void} no return value
         */
        $scope.loadTests = function () {
            $scope.tests = TestFactory.get({}, function () { },
                function (res) {
                if (res.status === 401) {
                    $location.path('/');
                }
            }
            );
        };
        $scope.loadTests();
        /**
         * Show the dialog (popup) for adding and editing a test
         * @param test      the test to be editted
         * @return {void}   no return value
         */
        $scope.showAdd = function (test) {
            ngDialog.open({
                template: 'partials/addExpects.html',
                controller: 'AddExpectsController',
                data: { test: test }
            }).closePromise.then(function (data) {
                $timeout(function () {
                    $scope.loadTests();
                }, 500);
            });
        };
    }
]);