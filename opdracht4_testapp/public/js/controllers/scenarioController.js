﻿/*global testApp*/
/**
 * controller for managing the scenarios
 * @param  {Object} $scope              Scopes provide separation between the model and the view
 * @param  {Object} $location           Angular object for redirecting users
 * @param  {Object} ScenarioFactory     Factory for using REST for scenario objects
 * @param  {Object} TestFactory         Factory for using REST for test objects
 * @param  {Object} ngDialog            Module for showing the dialog (popup)
 * @param  {Object} $timeout            Angular object for executing code after x miliseconds
 * @param  {Object} TestService         The service for executing tests on the backend
 * @return {void}   no return value
 */
testApp.controller('ScenarioController', ['$scope', '$location', 'ScenarioFactory', 'TestFactory', 'ngDialog', '$timeout', 'TestService',
    function ($scope, $location, ScenarioFactory, TestFactory, ngDialog, $timeout, TestService){
        'use strict';
        /**
         * array that holds all alerts displayed on the frontend
         * @type {Array}
         */
        $scope.alerts = [];
        /**
         * Get all scenario's and check if there are shared scenario's waiting to be accepted
         * @return {void} no return value
         */
        $scope.loadScenarios = function () {
            var i, scenario;
            $scope.scenarios = ScenarioFactory.get();
            $timeout(function () {
                console.log($scope.scenarios.doc.length);
                for (i = 0; i < $scope.scenarios.doc.length; i++) {
                    scenario = $scope.scenarios.doc[i];
                    if (scenario.shared === true && scenario.accepted === false) {
                        if (confirm('accept scenario: ' + scenario.name)) {
                            scenario.accepted = true;
                            ScenarioFactory.update({ id: scenario._id }, scenario, function (res) { });
                        } else {
                            $scope.delete(scenario);
                        }
                    }
                }
            }, 200);
        };
        $scope.loadScenarios();
        /**
         * Show the add or edit user dialog
         * @param scenario  The scenario that will be edited
         * @return {void} no return value
         */
        $scope.showAdd = function (scenario) {
            ngDialog.open({
                template: 'partials/addScenario.html',
                controller: 'AddScenarioController',
                data: { scenario: scenario }
            }).closePromise.then(function (data) {
                $timeout(function () {
                    $scope.loadScenarios();
                }, 200);
            });
        };
        /**
         * Delete a user and refresh scenario list
         * @return {void} no return value
         */
        $scope.delete = function (scenario) {
            ScenarioFactory.delete({ id: scenario._id }, function () {
                $scope.loadScenarios();
            });
        };
        /**
         * close alert by removing it from the array
         * @param index     the array[index] value of the alert to remove
         * @return {void}   no return value
         */
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
        /**
         * Execute a scenario
         * @param scenario  The scenario that will be executed
         * @return {void}   no return value
         */
        $scope.doScenario = function (scenario) {
            TestService.testScenario(scenario, function (alert) {
                $scope.alerts.push(alert);
            });
        };
        /**
         * Share a scenario with another user
         * @param scenario  The scenario that will be shared
         * @return {void}   no return value
         */
        $scope.share = function (scenario) {
            ngDialog.open({
                template: 'partials/shareScenario.html',
                controller: 'ShareScenarioController',
                data: { scenario: scenario }
            });
        };
    }
]);