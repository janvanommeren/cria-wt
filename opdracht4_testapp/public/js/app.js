﻿/*global angular*/
/**
 * the testApp module
 * @type {Object}
 */
var testApp = angular.module('testApp', 
    ['ngRoute', 'ngResource', 'ngCookies', 'ngAnimate', 'ngMessages', 'ui.bootstrap', 'ngDialog']);
/**
 * config variable for frontend
 */
var config = {
    backendUrl: 'http://localhost:8080'
};
/*jslint unparam: false*/
/**
 * all the routes for the module redRob
 * @param  {Object} $routeProvider Used for configuring routes.
 * @return {void} no return value              
 */
testApp.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'partials/login.html',
            controller: 'LoginController'
        }).when('/signup', {
            templateUrl: 'partials/signup.html',
            controller: 'SignupController'
        }).when('/users', {
            templateUrl: 'partials/users.html',
            controller: 'UserController'
        }).when('/tests', {
            templateUrl: 'partials/tests.html',
            controller: 'TestController'
        }).when('/scenarios', {
            templateUrl: 'partials/scenarios.html',
            controller: 'ScenarioController'
        }).when('/expects', {
            templateUrl: 'partials/expects.html',
            controller: 'ExpectsController'
        }).otherwise({
            redirectTo: '/'
        });
    }]);