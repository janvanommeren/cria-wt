﻿testApp.factory('TestFactory', ['$resource', function ($resource) {
        var actions = {
            'get': { method: 'get' },
            'save': { method: 'post' },
            'update': { method: 'put' },
            'query': { method: 'get', isarray: true },
            'delete': { method: 'delete' }
        };
        return $resource('/tests/:id', { id: '@id' }, actions);
        //return $resource('/tests/:id');
    }
]);