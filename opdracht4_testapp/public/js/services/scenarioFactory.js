﻿/*global testApp*/
/**
 * factory for performing REST request
 * @param $resrouce   Angular object for performing REST actions
 * @return {Promise}  The promise of the $http request
 */
testApp.factory('ScenarioFactory', ['$resource', function ($resource) {
      var actions = {
          'get': { method: 'get' },
          'save': { method: 'post' },
          'update': { method: 'put' },
          'query': { method: 'get', isarray: true },
          'delete': { method: 'delete' }
      };
      return $resource('/scenarios/:id', { id: '@id' }, actions);
    }
]);