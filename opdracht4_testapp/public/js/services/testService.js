﻿/*global testApp*/
/**
 * service for executing tests on the backend
 * @param  {Object} $http   Angular object for sending requests
 * @return {void}   no return value
 */
testApp.service('TestService', ['$http', '$timeout',
    function ($http, $timeout) {
        'use strict';
        /**
         * Execute a request on the backend
         * @param test          the test to be executed on the backend
         * @return {Promise}    the promise of the http request
         */
        this.testRequest = function (test) {
            var req;
            if (typeof test.duration === 'number' && test.duration % 1 === 0) {
                req = $http({
                    url: '/doTest',
                    method: 'POST',
                    data: test,
                    timeout: test.duration
                });
            } else {
                req = $http({
                    url: '/doTest',
                    method: 'POST',
                    data: test
                });
            }
            return req;
        };
        /**
         * Execute a serie of tests on the backend, it will re-execute itself for every test in the tests array
         * THIS FUNCTION SHOULD NOT BE CALLED MANUALLY!!
         * @param tests             the tests to be executed
         * @param index             the index of the array, doesn't need to be passed if called manually
         * @param cbAlertHandler    the alerthandler, doesn't need to be passed if called manually
         * @param data              the data from the previous request, doesn't need to be passed if called manually
         * @return {Object}         the data returnt from the http request
         */
        this.testNext = function (tests, index, cbAlertHandler, data) {
            var count, testData, self;
            if (tests.length === index) {
                return;
            }
            if (data && tests[index].data && tests[index].data.startsWith('&')) {
                testData = tests[index].data.substring(1);
                testData = testData.split('.');
                for (count in testData) {
                    data = data[testData[count]];
                    console.log(testData);
                    console.log(data);
                }
                tests[index].data = data;
            } else if (!tests[index].data || '' && data) {
                tests[index].data = data;
            }
            self = this;
            if (typeof tests[index].timeout === 'number' && tests[index].timeout % 1 === 0) {
                $timeout(function () { 
                   return self.executeTest(tests, index, cbAlertHandler, data);
                }, tests[index].timeout);
            } else {
                return self.executeTest(tests, index, cbAlertHandler, data);
            }
        };
        /**
         * Execute a test, this function is seperated because of the possibility to add a timeout()
         * THIS FUNCTION SHOULD NOT BE CALLED MANUALLY!!
         * @param tests             the tests to be executed
         * @param index             the index of the array, doesn't need to be passed if called manually
         * @param cbAlertHandler    the alerthandler, doesn't need to be passed if called manually
         * @param data              the data from the previous request, doesn't need to be passed if called manually
         * @return {Object}         the data return from the http request
         */
        this.executeTest = function (tests, index, cbAlertHandler, data) {
            var self = this;
            this.testRequest(tests[index]).then(function (data) {
                cbAlertHandler({ type: 'success', msg: data });
                self.performExpect(tests[index], data, cbAlertHandler);
                self.testNext(tests, index + 1, cbAlertHandler, data);
                return data;
            }, function (data) {
                cbAlertHandler({ type: 'danger', msg: data });
                self.performExpect(tests[index], data, cbAlertHandler);
                self.testNext(tests, index + 1, cbAlertHandler, data);
                return data;
            });
        };
        /**
         * Execute a request on the backend
         * @param scenario          the scenario to be executed on the backend
         * @param cbAlertHandler    the handler for the error / succes messages
         * @return {Object}         the data of the http request
         */
        this.testScenario = function (scenario, cbAlertHandler) {
            if (!cbAlertHandler || typeof cbAlertHandler != 'function') {
                console.log('No callback defined!');
                return;
            }
            return this.testNext(scenario.tests, 0, cbAlertHandler);
        };
        /**
         * Perform a expect on the results of a test
         * @param test              the test to be expected
         * @param data              the data to be expected
         * @param cbAlertHandler    the callback to be expected with the results, OPTIONAL
         * @return {Array}      returns if and which expect failed
         */
        this.performExpect = function (test, data, cbAlertHandler) {
            var i, expect, res, results, keys, first, last;
            results = [];
            if (typeof test.expects === 'undefined') {
                return results;
            }
            for (i = 0; i < test.expects.length; i++) {
                expect = test.expects[i];
                res = expect.split(' ');
                if (res[0] === 'length') {
                    results.push(eval('data.data.body.' + expect));
                } else if (res[0] === 'first') {
                    keys = Object.keys(data.data.body);
                    first = keys[0];
                    results.push(eval(expect.replace('first', data.data.body[first])));
                } else {
                    keys = Object.keys(data.data.body);
                    last = keys[keys.length - 1];
                    results.push(eval(expect.replace('last', data.data.body[last])));
                }
            }
            if (cbAlertHandler && typeof cbAlertHandler === 'function') {
                for (i = 0; i < results.length; i++) {
                    if (results[i]) {
                        cbAlertHandler({ type: 'success', msg: 'Expect ' + i + ': ' + results[i] });
                    } else {
                        cbAlertHandler({ type: 'danger', msg: 'Expect ' + i + ': ' + results[i] });
                    }
                }
            }
            return results;
        }
    }
]);