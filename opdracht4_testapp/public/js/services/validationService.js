﻿/*global testApp*/
/**
 * service for validation
 * Now uses, regular expressions, html5 validation can be implemented
 * https://docs.angularjs.org/api/ng/input/input%5Bemail%5D
 * @return {Object}   All functions within the service.
 */
testApp.factory('ValidationService', ['$http', function ($http) {
    'use strict';
    return {
        /**
         * Checks if id is a document id
         * @id      {String}    The id to be validated.
         * @return  {Boolean}   returns true if valid, false otherwise
         */
        isDocumentId: function (id) {
            var pattern = new RegExp('^[0-9a-fA-F]{24}$');
            return pattern.test(id);
        },
        /**
         * Checks if email is valid
         * @email   {String}    The email to be validated.
         * @return  {Boolean}   returns true if valid, false otherwise
         */
        isEmail: function (email) {
            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            //var pattern = new RegExp('^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)');
            //var pattern = new RegExp('/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/');
            return pattern.test(email);
        },
        /**
         * Checks if url is valid
         * @url     {String}    The url to be validated.
         * @return  {Boolean}   returns true if valid, false otherwise
         */
        isUrl: function (url) {
            var pattern = new RegExp('^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$');
            return pattern.test(url);
        }, 
        /**
         * Checks if user is valid
         * @user    {Object}    The user to be validated.
         * @return  {Boolean}   returns true if valid, false otherwise
         */
        isValidUser: function (user) {
            if (user._id && !this.isDocumentId(user._id)) {
                return false;
            }
            if (typeof user.name === "string" &&
                this.isEmail(user.email)) {
                return true;
            }
            return false;
        },
        /**
         * Checks if a test is valid
         * @test        {Object}    The email to be validated.
         * @checkDocId  {Boolean}   True if you want Id to be checked.
         * @return      {Boolean}   returns true if valid, false otherwise
         */
        isValidTest: function (test, checkDocId) {
            if (checkDocId && !this.isDocumentId(test._id) || 
                checkDocId && !this.isDocumentId(test.userId)) {
                return false;
            }
            return true;
        },
        /**
         * Checks if a test is valid
         * @scenario    {Object}    The scenario to be validated.
         * @return      {Boolean}   returns true if valid, false otherwise
         */
        isValidScenario: function (scenario) {
            if (this.isDocumentId(scenario._id) &&
                this.isDocumentId(scenario.userId)) {
                return true;
            }
            return false;
        }
    };   
}]);