﻿/*global testApp*/
/**
 * service for authenticating, create and logout a user
 * @param  {Object} $location           Angular object for redirecting users
 * @param  {Object} $http               Angular object for sending requests
 * @param  {Object} $rootScope          Angular variable for sharing vars
 * @param  {Object} $cookieStore        Angular object for handling cookies
 * @return {void}   no return value
 */
testApp.factory('AuthService', ['$http', '$location', '$rootScope', '$cookieStore',
    function ($http, $location, $rootScope, $cookieStore) {
        'use strict';
        /**
         * get the current loggedin user
         * @type {Object}
         */
        $rootScope.currentUser = $cookieStore.get('user');
        /**
         * remove the user cookie
         */
        $cookieStore.remove('user');

        return {
            /**
             * Login a user
             * @param           the user to login
             * @return {void}   no return value
             */
            login: function (user) {
                return $http.post('/login', user)
                .success(function (data) {
                    $rootScope.currentUser = data;
                    $location.path('/tests');
                }).error(function () {
                });
            },
            /**
             * Signup a user
             * @param           the user to signup
             * @return {void}   no return value
             */
            signup: function (user) {
                return $http.post('/signup', user)
                .success(function () {
                    $location.path('/login');
                }).error(function (response) {
                });
            },
            /**
             * Logout a user
             * @return {void}   no return value
             */
            logout: function () {
                return $http.get('/logout').success(function () {
                    $rootScope.currentUser = null;
                    $cookieStore.remove('user');
                    $location.path('/');
                });
            }
        };
    }
]);