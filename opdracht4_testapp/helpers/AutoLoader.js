﻿/*jslint nomen:true, devel:true, node:true, stupid: true*/
/*global module, require, __dirname*/
var mongoose = require('mongoose'), 
    fs = require('fs');
/**
 * exports this AuthHelper to the app.
 * @param  {Ojbect} app    the application to export this set of function to
 * @param  {Ojbect} ensureAuthenticated the variable which checks if the user is authenticated
 * @return {Object}        returns alle the public functions which can be used in this app
 */
module.exports = function (app, ensureAuthenticated) {
    'use strict';
    /**
     * the path to the controllers folder
     * @type {String}
     */
    var controllerPath = __dirname + '/../controllers';
    /**
    * the path to the routes folder
    * @type {String}
    */
    var routesPath = __dirname + '/../routes';
    /**
    * the path to the models folder
    * @type {String}
    */
    var modelsPath = __dirname + '/../models';
    /**
     * loads all the models from the folder
     * @return {void} returns no value
     */
    function loadModels() {
        var models = fs.readdirSync(modelsPath);
        models.forEach(function (file) {
            require(modelsPath + '/' + file);
        });
    }
    /**
     * creates a mongo model from the found models in the folder
     * @param  {String} restName the name of the mongo model
     * @return {Object}          returns the found model
     */
    function createMongoModel(restName) {
        try {
            var model = mongoose.model(restName);
            return model;
        } catch (exception) {
            console.log('AutoLoader: mongoose model not found or invalid: ' + exception);
            return null;
        }
    }
    /**
     * loads all the controllers, found in the folder
     * @param  {Objet} restName modelname
     * @param  {Object} model    the found model
     * @return {Object}          returns the found model
     */
    function loadController(restName, model) {
        var controllerName = restName + 'Controller.js';
        try {
            var controller = require(controllerPath + '/' + controllerName)(mongoose, model);
            return controller;
        } catch (exception) {
            console.log('AutoLoader: controller not found or invalid: ' + exception);
            return null;
        }
    }
    /**
     * loads all the routes, found in the folder
     * @param  {String} file                found route file
     * @param  {Object} app                 Nodejs App
     * @param  {Object} controller          Used Controller
     * @param  {Object} ensureAuthenticated Checks if the user is authenticated
     * @return {Object}                     Returns a route
     */
    function loadRoute(file, app, controller, ensureAuthenticated) {
        try {
            require(routesPath + '/' + file)(app, controller, ensureAuthenticated);
        } catch (exception) {
            console.log('AutoLoader: invalid route: ' + exception);
        }
    }
    /**
    * executes the autoloader
    * loads all the models, controller and routes
    * @return {void} returns not a value
    */
    return {
        autoLoad: function () {
            loadModels();
            var routes = fs.readdirSync(routesPath);
            routes.forEach(function (file) {
                var restName = file.replace('Routes.js', '');
                var model = createMongoModel(restName);
                var controller = loadController(restName, model);
                loadRoute(file, app, controller, ensureAuthenticated);
            });
        }
    };
};