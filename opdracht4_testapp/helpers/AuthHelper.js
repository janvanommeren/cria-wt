﻿/*jslint nomen:true, unparam: true*/
/*global module, require, __dirname*/
/**
 * exports this AuthHelper to the app.
 * @param  {Ojbect} app    the application to export this set of function to
 * @param  {Ojbect} config the used configuration
 * @return {Object}        returns alle the public functions which can be used in this app
 */
module.exports = function (app, config) {
    'use strict';
    /**
     * Authentication module
     * @type {Object}
     */
    var passport = require('passport');
    var session = require('express-session');
    var LocalStrategy = require('passport-local').Strategy;
    var mongoose = require('mongoose');
    var userSchema = require(__dirname + '/../models/User.js');
    var User = mongoose.model('User');
    /**
     * serialize a user to string
     * @param  {Object} user  a user of the application
     * @param  {Object} done) function called after sirializing              
     * @return {void}       void
     */
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });
    /**
     * [description]
     * @param  {int} id         id of the user
     * @param  {Function} done  function called on error
     * @return {void}           void
     */
    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });
    /**
     * Use a local strategy for authentication
     * @param  {string} email    email as username
     * @param  {string} password password for authentication
     * @param  {Function} done   Function called on error
     * @return {Function}        Returns the done function as callback.
     */
    passport.use(new LocalStrategy({ usernameField: 'email' }, function (email, password, done) {
        User.findOne({ email: email }, function (err, user) {
            if (err) return done(err);
            if (!user) return done(null, false);
            user.comparePassword(password, function (err, isMatch) {
                if (err) return done(err);
                if (isMatch) return done(null, user);
                return done(null, false);
            });
        });
    }));
    app.use(session({
        secret: config.secret,
        saveUninitialized: true,
        resave: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    /**
     * formats the user a a json string
     * @param  {Object} req   Reseived request
     * @param  {Object} res   Sended response
     * @param  {Function} next Next function to be called
     * @return {void}       no return value
     */
    app.use(function (req, res, next) {
        if (req.user) {
            res.cookie('user', JSON.stringify(req.user));
        }
        next();
    });
    
    //set routes
    /**
     * formats the user a a json string
     * @param  {Object} req   Reseived request
     * @param  {Object} res   Sended response
     * @param  {Function} next Next function to be called
     * @return {void}       no return value
     */
    app.post('/login', passport.authenticate('local'), function (req, res) {
        res.cookie('user', JSON.stringify(req.user));
        res.send(req.user);
    });
    /**
     * adds a route and creates a user
     * @param  {Object} req   Reseived request
     * @param  {Object} res   Sended response
     * @param  {Function} next Next function to be called
     * @return {void}       no return value
     */
    app.post('/signup', function (req, res, next) {
        var user = new User({
            email: req.body.email,
            password: req.body.password
        });
        user.save(function (err) {
            if (err) return next(err);
            res.send(200);
        });
    });
    /**
     * adds a route and logs the user out
     * @param  {Object} req   Reseived request
     * @param  {Object} res   Sended response
     * @param  {Function} next Next function to be called
     * @return {int}       returns a 200 http code
     */
    app.get('/logout', function (req, res, next) {
        req.logout();
        res.send(200);
    });

    return {
        app: app,
        /**
         * checks if the user is authenticated
         * @param  {Object}   req  the reseived request
         * @param  {Object}   res  the sended response
         * @param  {Function} next the next function to be called
         * @return {void}        returns no value
         */
        notAuthenticated: function (req, res, next) {
            next();
        },
        /**
         * checks if the user is authenticated
         * @param  {Object}   req  the reseived request
         * @param  {Object}   res  the sended response
         * @param  {Function} next the next function to be called
         * @return {void}        returns no value
         */
        ensureAuthenticated: function (req, res, next) {
            if (req.isAuthenticated()) next();
            else res.send(401);
        },
        /**
         * checks if a user is an administrator
         * @param  {Object}   req  the reseived request
         * @param  {Object}   res  the sended response
         * @param  {Function} next the next function to be called
         * @return {void}        returns no value
         */
        ensureAdmin: function (req, res, next) {
            if (typeof req.user === 'undefined' || !req.user || 
                typeof req.user._doc === 'undefined' || !req.user._doc) {
                res.send(401);
            }
            User.findById(req.user._doc._id, function (err, user) {
                if (!err && req.user.password === user.password && req.isAuthenticated() && user.isAdmin) {
                    next();
                } else {
                    res.send(401);
                }
            });
        }
    };
};