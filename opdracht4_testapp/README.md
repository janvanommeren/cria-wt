﻿# TestApp

## Hoe kan je deze applicatie gebruiken?
* Node.js dient geinstalleerd te zijn.
* MongoDB dient geinstalleerd te zijn.
* Voer /database/runServer.bat uit (laat deze openstaan).
* Voor /database/runUpdate.bat uit.
* Installeer alle packages via de Node Package Manager (NPM).
* Start server.js

Het frontend van de applicatie is te vinden in de map /public.
Het backend is te vinden op de root (/) van de applicatie.

## Frontend
Voor elke view is een controller aangemaakt, de views staan in /public/partials en de bij behorende controllers zijn te vinden in de map /public/controllers.
De services waar de applicatie gebruik van maakt bevinden zich in de map: /public/services

## API documentatie
De applicatie bestaat uit 3 api's die aangeroepen kunnen worden: UserController, TestController, ScenarioController

### UserController
De UserController wordt gebruikt om CRUD operaties uit te voeren op een model.
De routes waarmee je deze controller kunt bereiken zijn als volgt:

url:			Method:		function:				Omschrijving:
/users/:id		GET			UserController.find		Haal een gebruiker op met id :id
/users			GET			UserController.list		Haal alle gebruikers op
/users/:id		PUT			UserController.update	Update de user met id :id
/users			POST		UserController.create	Maak een nieuwe user aan
/users/:id		DELETE		UserController.delete	Verwijder de user met id :id