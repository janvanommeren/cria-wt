﻿describe('UserController', function () {
    
    var request = require('request');
    var request = request.defaults({ jar: true });
    var url = 'http://localhost:8080/';
    var testUser = {
        email: 'user1@outlook.com',
        password: 'user1'
    };

    beforeEach(function (done) {
        request.post(url + 'login', { form: { email: 'janvanommeren@hotmail.com', password: 'test' } }, 
        function (err, httpResponse, body) {
            done();
        });
    });

    it('Should return a list of users', function (done) {
        request(url + 'users', function (error, response, body) {
            expect(JSON.parse(body).doc[0].email).toEqual('janvanommeren@hotmail.com');
            done();
        });
    });
    
    it('Should create a test user', function (done) {
        var options = {
            url: url + 'users',
            body: testUser,
            json: true
        };
        request.post(options, function (error, response, body) {
            expect(body.doc.email).toEqual(testUser.email);
            testUser = body.doc;
            done();
        });
    });

    it('Should return the test user', function (done) {
        request(url + 'users/' + testUser._id, function (error, response, body) { 
            expect(JSON.parse(body).doc.email).toEqual(testUser.email);
            done();
        });
    });

    it('Should delete the test user', function (done) {
        request.del(url + 'users/' + testUser._id, function (error, response, body) { 
            expect(error).toBeNull();
            done();
        });
    });

});