﻿/*global module*/
/**
 * Exported routes for the scenarios
 * @param  {Object} app             the used app
 * @param  {Object} controller the used controller
 * @param  {Object} authHelper      the helper for authentication
 * @return {void}                 no return value
 */
module.exports = function (app, controller, authHelper) {
    'use strict';
    app.get('/scenarios', authHelper.ensureAuthenticated, controller.list);
    app.get('/scenarios/:id', authHelper.ensureAuthenticated, controller.find);
    app.post('/scenarios', authHelper.ensureAuthenticated, controller.create);
    app.put('/scenarios/:id', authHelper.ensureAuthenticated, controller.update);
    app.delete('/scenarios/:id', authHelper.ensureAuthenticated, controller.delete);
}