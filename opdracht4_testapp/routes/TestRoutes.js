/*global module*/
/**
* Exported routes for the tests
* @param  {Object} app             the used app
* @param  {Object} controller the used controller
* @param  {Object} authHelper      the helper for authentication
* @return {void}                 no return value
*/
module.exports = function (app, controller, authHelper) {
  'use strict';
  app.get('/tests', authHelper.ensureAuthenticated, controller.list);
  app.get('/tests/:id', authHelper.ensureAuthenticated, controller.find);
  app.post('/tests', authHelper.ensureAuthenticated, controller.create);
  app.put('/tests/:id', authHelper.ensureAuthenticated, controller.update);
  app.delete('/tests/:id', authHelper.ensureAuthenticated, controller.delete);
  app.post('/doTest', authHelper.ensureAuthenticated, controller.doRequest);

    app.get('/testtest', authHelper.notAuthenticated, function (req, res) {
        var retObj = { test: [ 3, 10, 20, 100] };
        return res.send(retObj);
    });
    app.post('/testtest2', authHelper.notAuthenticated, function (req, res) {
        console.log(req.body);
        return res.send(req.body);
    });
};