﻿/*global module*/
/**
 * Exported routes for the users
 * @param  {Object} app             the used app
 * @param  {Object} controller      the used controller
 * @param  {Object} authHelper      the helper for authentication
 * @return {void}   no return value
 */
module.exports = function (app, controller, authHelper) {
  'use strict';
  app.get('/users', authHelper.ensureAuthenticated, controller.list);
  app.get('/users/:id', authHelper.ensureAdmin, controller.find);
  app.post('/users', authHelper.ensureAdmin, controller.create);
  app.put('/users/:id', authHelper.ensureAdmin, controller.update);
  app.delete('/users/:id', authHelper.ensureAdmin, controller.delete);
};