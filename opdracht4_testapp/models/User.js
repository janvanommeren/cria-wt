﻿/*jslint node: true*/
/*global require*/
/**
 * bcrypt module
 * @type {Object}
 */
var bcrypt = require('bcryptjs');
/**
* Mongoose module
* @type {Object}
*/
var mongoose = require('mongoose');
/**
* Mongoose schema
* @type {Object}
*/
var Schema = mongoose.Schema;
/**
* The user model
* @type {Schema}
*/
var User = Schema({
    name: { type: String, required: false },
    email: { type: String, required: true },
    password: { type: String, required: true },
    isAdmin: { type: Boolean, default: false }
});
/**
 * Validates the user model
 * @param  {the input for the validation} val
 * @return {boolean} valid of not valid
 */
User.path('name').validate(function (val) {
    return (val !== undefined && val !== null && val.length >= 3);
}, 'Invalid name');
/**
 * Excecutes before the user is saved
 * @param  {Function} next next excecuted function
 * @return {Function}      returns an error or a user
 */
User.pre('save', function (next) {
    var user = this;
    if (!user.isModified('password')) return next();
    bcrypt.genSalt(10, function (err, salt) {
        if (err) return next(err);
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
});
/**
 * Compares the passwords of the user with the password in the database
 * @param  {Object}   candidatePassword the password used in the form
 * @param  {Function} cb                return function
 * @return {Function}                   returns a function which checks the passwords
 */
User.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};
/**
 * name of the model
 * @type {String}
 */
var modelName = 'User';
var collectionName = 'users';
mongoose.model(modelName, User, collectionName);