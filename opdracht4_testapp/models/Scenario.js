﻿/*jslint node: true*/
/*global require*/
/**
* Mongoose module
* @type {Object}
*/
var mongoose = require('mongoose');
/**
* Mongoose schema
* @type {Object}
*/
var Schema = mongoose.Schema;
/**
* The test model
* @type {Schema}
*/
var Scenario = Schema({
    name: { type: String, required: true },
    tests: { type: Array, required: true },
    userId: { type: String, required: true },
    accepted: { type: Boolean, default: false, required: false },
    shared: { type: Boolean, default: false, required: false }
});
/**
 * name of the model
 * @type {String}
 */
var modelName = 'Scenario';
var collectionName = 'scenarios';
mongoose.model(modelName, Scenario, collectionName);