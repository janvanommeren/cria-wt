﻿/*jslint node: true*/
/*global require*/
/**
* Mongoose module
* @type {Object}
*/
var mongoose = require('mongoose');
/**
* Mongoose schema
* @type {Object}
*/
var Schema = mongoose.Schema;
/**
* The test model
* @type {Schema}
*/
var Test = Schema({
    name: { type: String, required: true },
    headers: { type: Object, required: false },
    url: { type: String, required: true },
    method: { type: String, required: true },
    data: { type: Object, required: false },
    userId: { type: String, required: true },
    duration: { type: Number, required: false },
    timeout: { type: Number, required: false },
    expects: { type: Array, required: false }
});
/**
 * name of the model
 * @type {String}
 */
var modelName = 'Test';
var collectionName = 'tests';
mongoose.model(modelName, Test, collectionName);