# README

Author:   Jan van Ommeren
Version:  0.9
Since:    30-09-2014

## Inleiding
De applicatie begint bij het bestand main.js, dit bestand is het aanroeppunt voor de photoviewer. In dit bestand wordt een object gedefineerd genaamd PhotoViewer, dit object functioneert als namespace zodat er geen variabelen of functies met de zelfde naam bestaan. Vervolgens wordt de start functie gedefinceerd, deze functie is het startpunt van de applicatie, de functie kan gebruikt worden met de volgende opties: 
`PhotoViewer.start({
    container: 'gallery', //container waar de photoviewer in gebouwd wordt
    album: album, //de foto's die in de photoviewer geladen worden
    columnsInRow: 5 //het aantal kolommen gewenst in een rij
});`
De album parameter kan als volgt worden gedefineerd:
`album =
    [{
      url: 'images/0006787.jpg', //pad naar de afbeelding
      description: 'Shadows to the sea...', //beschrijving van de afbeelding
      photographer: 'Sander', //photograaf
      date: new Date('12-8-1999') //datum waarop de foto is genomen
    }, 
    {
      url: 'images/0004713.jpg'
    }];`
Vervolgens zullen de opties worden gevalideert, mocht dit mislukken dan verschijnt hier een bericht van in de javascript console.

## Gebruikte Technieken

### Prototyping


## Objecten
Hieronder zullen de gebruikte objecten en events beschreven worden per onderdeel.

### main.js
Alle functies in het bestand main.js hebben een verandwoordelijkheid met betrekking tot het starten van de applicatie.

Function: `PhotoViewer.start(options);`
Description: Deze functie moet gebruikt worden om de photoviewer te starten.
Param options: In dit object staan de opties van de applicatie voor meer toelichting, zie: Inleiding.
Return: Void

Function: `PhotoViewer.validateOptions(options);`
Description: Deze functie wordt gebruikt om de options variabele te valideren.
Param options: In dit object staan de opties van de applicatie voor meer toelichting, zie: Inleiding.
Return: Options object gevalideert.

### pageBuilder.js
Alle functies in het bestand pageBuilder.js hebben betrekking tot het bouwen van de pagina en het dynamisch inladen van de afbeeldingen.

Function: `PhotoViewer.PageBuilder.createPhotoContainer(columnsInRow, album, container)`
Description: Deze functie bouwt de PhotoViewer, deze functie dient enkel maar aangeroepen te worden bij het opstarten.
Param columnsInRow: Het aantal gewenste horizontale kolommen.
Param album: De array met afbeelding.
Param: container: De ID van de container waarbinnen de PhotoViewer gebouwt dient te worden.
Return: Void

Function: `PhotoViewer.PageBuilder.reloadWithoutImage(imageId)`
Description: Deze functie herbouwt de pagina zonder de afbeelding met het id die als parameter wordt meegegeven.
Param imageId: Het id van het te verwijderen image.
Return Void

Function: `PhotoViewer.PageBuilder.generateTransformVal()`
Description: Deze functie genereert een willekeurig getal tussen de -3 en de 3, dit wordt gebruikt bij het bepalen van de CSS3 transform waarde om de foto's te kantelen.
Return: Integer

### eventManager.js
Alle functies in het bestand eventViewer.js hebben betrekking tot het zetten en weghalen van events.

Function: `PhotoViewer.EventManager.setOnclickImages()`
Description: Deze functie dient aangeroepen te worden als 'init', deze functie bepaald wat er moet gebeuren zodra er op een afbeelding geklikt wordt, ook worden er andere functies van de eventManager aangesproken om andere events aan te maken of te verwijderen. 
Return: Void

Function: `setOnclickSingleImage(images, toSet)`
Description: Deze functie wordt gebruikt om verschillende events te setten op de afbeelding waarvan het id wordt meegegeven in de toSet parameter.
Param images: Alle afbeeldingen die behoren tot de photoviewer.
Param toSet: Het id van de foto waar de onClick op geset moet worden.
Return: Void

Function: `PhotoViewer.EventManager.setKeyEvents()`
Description: Deze functie bepaald wat er gebeurd zodra er een toets wordt ingedrukt, op dit moment bied de PhotoViewer enkel ondersteuning voor de pijltjestoetsen om te navigeren door de foot's en de spatiebalk om in en uit te zoomen.
Return: Void

Function: `PhotoViewer.EventManager.removeOnclickImages()`
Description: Deze functie verwijderd de onclick events van alle afbeeldingen.
Return: Void

Function: `PhotoViewer.EventManager.setRightClickImage()`
Description: Deze functie verwijderd de afbeelding uit de array met afbeeldingen.
Return: Void

Function: `PhotoViewer.EventManager.setZoomOutClick()`
Description: Deze functie zorgt er voor dat zodra er is ingezoomed op een foto en er naast de foto wordt geklikt de pagina weer uitzoomt.
Return: Void

### zoomer.js
Het zoomer.js bestand bevat functies met betrekking tot het in en uit zoomen. Deze functie is zelf uitvoerend wat inhoudt dat ik hier het Singleton pattern op heb toegepast. Dit heb ik gedaan omdat er altijd maar een instantie van dit object mag zijn.

Function: `doZoom()`
Description: Deze functie past de style van de html elementen aan zodat elke kolom met daarin een foto de breedte en de hoogte heeft van het scherm van de gebruiker.
Return: Void

Function: `unZoom()`
Description: Deze functie draait alle aangepaste styles van de functie `doZoom()` terug naar de orignele staat.
Return: Void

Function: `PhotoViewer.Zoomer.toggleZoom(callback)`
Description: Deze functie maakt gebruik van de `doZoom()` en `unZoom()` functies om de pagina in te zoomen als hij uitgezoomt is en om uit te zoomen als de pagina ingezoomt is.
Return: Void

Function: `PhotoViewer.Zoomer.pageIsZoomed()`
Description: Deze functie returnt de instance variabel die de waarde true bevat als de pagina is ingezoomt en false als de pagina is uitgezoomt.
Return: Boolean

### scroller.js
Het scroller.js bestand bevat alle functies met betrekking tot het scrollen binnen de pagina, er wordt vooral van de scroller gebruik gemaakt door de EventManager klasse. Op de scroller wordt het Singleton pattern toegepast omdat er altijd maar een instantie van de scroller mag zijn.

Function: `PhotoViewer.Scroller.scrollToImage(imageId)`
Param imageId: Het id van de afbeelding waar naar toe gescrolt moet worden.
Description: Deze functie gebruikt een interval om de pagina te laten scrollen naar de plaats van de afbeelding met als id imageId.
Return: Void

Function: `PhotoViewer.Scroller.scrollLeft(columnsInRow)`
Param columnsInRow: Het aantal horizontale kolommen.
Description: Deze functie bepaalt naar welke afbeelding gescrollt wordt op het moment dat er naar links gescrolt moet worden.
Return: Void

Function: `PhotoViewer.Scroller.scrollRight(columnsInRow)`
Param columnsInRow: Het aantal horizontale kolommen.
Description: Deze functie bepaalt naar welke afbeelding gescrollt wordt op het moment dat er naar rechts gescrolt moet worden.
Return: Void

Function: `PhotoViewer.Scroller.scrollUp(columnsInRow, amountOfImages)`
Param columnsInRow: Het aantal horizontale kolommen.
Param amountOfImages: Het totaal aantal afbeeldingen in de PhotoViewer.
Description: Deze functie bepaalt naar welke afbeelding gescrollt wordt op het moment dat er omhoog gescrolt moet worden.
Return: Void

Function: `PhotoViewer.Scroller.scrollDown(columnsInRow, amountOfImages)`
Param columnsInRow: Het aantal horizontale kolommen.
Param amountOfImages: Het totaal aantal afbeeldingen in de PhotoViewer.
Description: Deze functie bepaalt naar welke afbeelding gescrollt wordt op het moment dat er naar beneden gescrolt moet worden.
Return: Void