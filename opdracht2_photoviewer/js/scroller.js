/*global PhotoViewer*/
/*global screen*/
/*global window*/
/*global setInterval*/
/*global clearInterval*/
PhotoViewer.Scroller = (function () {
    'use strict';
    var currentImage = 0, isScrolling = false, currentActive = 0;

    function scrollToImage(imageId) {
        if (isScrolling === true) {
            return;
        }
        var xPos, yPos, scrolledX, scrolledY, interval;
        xPos = 0;
        yPos = 0;
        if (imageId > (PhotoViewer.options.columnsInRow - 1)) {
            xPos = (imageId % PhotoViewer.options.columnsInRow) * screen.width;
            yPos = Math.floor(imageId / PhotoViewer.options.columnsInRow) * screen.height;
        } else {
            xPos = imageId * screen.width;
        }
        scrolledX = window.scrollX;
        scrolledY = window.scrollY;
        isScrolling = true;
        interval = setInterval(function () {
            if (imageId >= currentImage) {
                if (scrolledX <= xPos) {
                    scrolledX += 10;
                }
                if (scrolledY <= yPos) {
                    scrolledY += 10;
                }
                if (scrolledY >= yPos && scrolledX >= xPos) {
                    clearInterval(interval);
                    currentImage = imageId;
                    isScrolling = false;
                }
            } else {
                if (scrolledX > xPos) {
                    scrolledX -= 10;
                }
                if (scrolledY > yPos) {
                    scrolledY -= 10;
                }
                if (scrolledY <= yPos && scrolledX <= xPos) {
                    clearInterval(interval);
                    currentImage = imageId;
                    isScrolling = false;
                }
            }
            window.scrollTo(scrolledX, scrolledY);
        }, 1);
        currentActive = imageId;
    }

    function getCurrentImage() {
        if (currentImage) {
            return currentImage;
        }
        return 0;
    }

    function scrollLeft(columnsInRow) {
        if (isScrolling === true) {
            return;
        }
        if (getCurrentImage() % columnsInRow === 0) {
            scrollToImage(currentImage + (columnsInRow - 1));
        } else {
            scrollToImage(currentImage - 1);
        }
    }

    function scrollRight(columnsInRow) {
        if (isScrolling === true) {
            return;
        }
        if ((currentImage + 1) % columnsInRow === 0) {
            scrollToImage(currentImage - (columnsInRow - 1));
        } else {
            scrollToImage(currentImage + 1);
        }
    }

    function scrollUp(columnsInRow, amountOfImages) {
        if (isScrolling === true) {
            return;
        }
        var leftover = amountOfImages % columnsInRow, currentimage = getCurrentImage();
        if (leftover > 0 && currentimage > leftover) {
            scrollToImage(amountOfImages);
        } else if ((currentImage - columnsInRow) < 0) {
            scrollToImage((amountOfImages - columnsInRow) + currentImage);
        } else {
            scrollToImage(currentImage - columnsInRow);
        }
    }

    function scrollDown(columnsInRow, amountOfImages) {
        if (isScrolling === true) {
            return;
        }
        if ((amountOfImages - currentImage) < columnsInRow) {
            var rowAmount, columnsInRowsAbove;
            rowAmount = Math.ceil(amountOfImages / columnsInRow);
            columnsInRowsAbove = (rowAmount - 1) * columnsInRow;
            scrollToImage(currentImage - columnsInRowsAbove);
        } else {
            scrollToImage(currentImage + columnsInRow);
        }
    }

    return {
        scrollToImage: function (imageId) {
            scrollToImage(imageId);
        },
        scrollLeft: function (columnsInRow) {
            scrollLeft(columnsInRow);
        },
        scrollRight: function (columnsInRow) {
            scrollRight(columnsInRow);
        },
        scrollUp: function (columnsInRow, amountOfImages) {
            scrollUp(columnsInRow, amountOfImages);
        },
        scrollDown: function (columnsInRow, amountOfImages) {
            scrollDown(columnsInRow, amountOfImages);
        },
        setCurrentImage: function (id) {
            currentImage = id;
        },
        getCurrentActive: function () {
            return currentActive;
        }
    };
}());