/*global PhotoViewer*/
/*global document*/
/*global screen*/
/*global setTimeout*/
PhotoViewer.Zoomer = (function () {
    'use strict';
    var isZoomed = false;

    function doZoom() {
        var photoContainer, rows, rowCount, columns, columnCount, image, text;
        photoContainer = document.getElementById('photoContainer');
        rows = photoContainer.getElementsByClassName('row');
        photoContainer.style.width = (screen.width * PhotoViewer.options.columnsInRow + 50) + 'px';
        photoContainer.style.height = (screen.height * rows.length) + 'px';
        text = photoContainer.getElementsByClassName('txtTopLeft');
        for (rowCount = 0; rowCount < rows.length; rowCount += 1) {
            rows[rowCount].style.height = screen.height + 'px';
            columns = rows[rowCount].getElementsByClassName('column');
            for (columnCount = 0; columnCount < columns.length; columnCount += 1) {
                text[columnCount].style.display = 'inline';
                columns[columnCount].style.width = screen.width + 'px';
                columns[columnCount].style.height = 'auto';
                image = columns[columnCount].getElementsByTagName('img')[0];
                image.style.maxWidth = screen.width + 'px';
                image.style.maxHeight = screen.height + 'px';
            }
        }
    }

    function unZoom() {
        var photoContainer, rows, images, rowCount, columns, columnCount, image, imageCount, text;
        photoContainer = document.getElementById('photoContainer');
        rows = photoContainer.getElementsByClassName('row');
        text = photoContainer.getElementsByClassName('txtTopLeft');
        images = [];
        photoContainer.style.width = '625px';
        photoContainer.style.height = 'auto';
        photoContainer.style.transition = 'width 0.5s';
        for (rowCount = 0; rowCount < rows.length; rowCount += 1) {
            rows[rowCount].style.height = 'auto';
            columns = rows[rowCount].getElementsByClassName('column');
            for (columnCount = 0; columnCount < columns.length; columnCount += 1) {
                columns[columnCount].style.width = '115px';
                columns[columnCount].style.height = 'auto';
                text[columnCount].style.display = 'none';
                image = columns[columnCount].getElementsByTagName('img')[0];
                image.style.maxWidth = '100px';
                image.style.maxHeight = '100px';
                image.style.transition = 'max-height 0.5s';
                images[images.length] = image;
            }
        }
        setTimeout(function () {
            photoContainer.style.transition = 'width 0s';
            for (imageCount = 0; imageCount < images.length; imageCount += 1) {
                images[imageCount].style.transition = 'max-height 1.5s';
            }
        }, 500);
        PhotoViewer.Scroller.setCurrentImage(0);
    }

    return {
        toggleZoom: function (callback) {
            if (isZoomed === false) {
                doZoom();
                if (typeof callback === "function") {
                    callback();
                }
                isZoomed = true;
            } else {
                unZoom();
                isZoomed = false;
            }
        },
        pageIsZoomed: function () {
            return isZoomed;
        }
    };
}());