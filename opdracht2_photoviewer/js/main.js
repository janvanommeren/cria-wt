var PhotoViewer = {};

PhotoViewer.start = function (options) {
    'use strict';
    PhotoViewer.options = PhotoViewer.validateOptions(options);
    if (PhotoViewer.options !== false) {
        var builder = new PhotoViewer.PageBuilder(PhotoViewer.options.columnsInRow,
				PhotoViewer.options.album, PhotoViewer.options.container), 
			evtManager = new PhotoViewer.EventManager(PhotoViewer.options.container);
        builder.createPhotoContainer();
        evtManager.setOnclickImages();
        evtManager.setKeyEvents();
    }
};

PhotoViewer.validateOptions = function (options) {
    'use strict';
    /*jslint browser:true*/
    /*global console*/
    options.container = document.getElementById(options.container);
    if (options.container === null) {
        console.log('container not found');
    } else if (Array.isArray(options.album.length) && options.album.length > 0) {
        console.log('invalid album');
    } else if (typeof options.columnsInRow !== 'number' || options.columnsInRow % 1 !== 0) {
        console.log('invalid columnsInRow');
    } else {
        return options;
    }
    return false;
};

window.onload = function () {
    'use strict';
    /*global album*/
    PhotoViewer.start({
        container: 'gallery',
        album: album,
        columnsInRow: 5
    });
};