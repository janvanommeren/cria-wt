/*global PhotoViewer*/
/*global document*/
PhotoViewer.PageBuilder = function (columnsInRow, album, container) {
    'use strict';
    this.columnsInRow = columnsInRow;
    this.album = album;
    this.container = container;
};

PhotoViewer.PageBuilder.prototype.createPhotoContainer = function () {
    'use strict';
    var rowAmount, photoContainer, row, column, vAlignHelper, image, rowCount, columnCount, text;
    rowAmount = Math.ceil(this.album.length / this.columnsInRow);
    photoContainer = document.createElement('div');
    photoContainer.id = 'photoContainer';
    for (rowCount = 0; rowCount < rowAmount; rowCount += 1) {
        row = document.createElement('div');
        row.className = 'row';
        for (columnCount = 0; columnCount < this.columnsInRow; columnCount += 1) {
            column = document.createElement('div');
            column.className = 'column';
            if (columnCount + rowCount * this.columnsInRow < this.album.length) {
                vAlignHelper = document.createElement('span');
                vAlignHelper.className = 'vAlignHelper';
                column.appendChild(vAlignHelper);
                text = document.createElement('p');
                text.className = 'txtTopLeft';
                text.innerHTML = this.album[columnCount + rowCount * this.columnsInRow].url;
                console.log();
                column.appendChild(text);
                image = document.createElement('img');
                image.setAttribute('alt', this.album[columnCount + rowCount * this.columnsInRow].url);
                image.setAttribute('src', this.album[columnCount + rowCount * this.columnsInRow].url);
                image.dataset.id = columnCount + rowCount * this.columnsInRow;
                image.style.transform = 'rotate(' + this.generateTransformVal() + 'deg)';
                column.appendChild(image);
                row.appendChild(column);
            }
        }
        photoContainer.appendChild(row);
    }
    this.container.appendChild(photoContainer);
};

PhotoViewer.PageBuilder.prototype.reloadWithoutImage = function (imageId) {
    'use strict';
    this.album.splice(imageId, 1);
    var photoContainer = document.getElementById('photoContainer');
    this.container.removeChild(photoContainer);
    this.createPhotoContainer();
};

PhotoViewer.PageBuilder.prototype.generateTransformVal = function () {
    'use strict';
    var random = Math.floor((Math.random() * 3) + 1);
    if (Math.floor((Math.random() * 2) + 1) === 1) {
        return random * -1;
    }
    return random;
};