/*global PhotoViewer*/
/*global window*/
PhotoViewer.EventManager = function (container) {
    'use strict';
    this.container = container;
};

PhotoViewer.EventManager.prototype.setOnclickImages = function () {
    'use strict';
    var images = this.container.getElementsByTagName('img'),
        toSet = 0;
    for (toSet = 0; toSet < images.length; toSet += 1) {
        this.setOnclickSingleImage(images, toSet);
    }
    this.setRightClickImage();
};

PhotoViewer.EventManager.prototype.setOnclickSingleImage = function (images, toSet) {
    'use strict';
    var self = this, id = 0;
    images[toSet].onclick = function () {
        id = parseInt(this.dataset.id, 0);
        PhotoViewer.Zoomer.toggleZoom(PhotoViewer.Scroller.scrollToImage(id));
        self.removeOnclickImages();
        self.setZoomOutClick();
        self.removeRightClickImage();
    };
};

PhotoViewer.EventManager.prototype.setKeyEvents = function () {
    'use strict';
    var self = this;
    window.onkeydown = function (event) {
        if (event.keyCode === 32) {
            if (PhotoViewer.Zoomer.pageIsZoomed() === true) {
                self.container.onclick = null;
                self.setOnclickImages();
            } else {
                self.removeOnclickImages();
                self.setZoomOutClick();
            }
            
            PhotoViewer.Zoomer.toggleZoom(PhotoViewer.Scroller.scrollToImage(PhotoViewer.Scroller.getCurrentActive()));
        } else if (PhotoViewer.Zoomer.pageIsZoomed() === true && event.keyCode === 37) {
            PhotoViewer.Scroller.scrollLeft(PhotoViewer.options.columnsInRow);
        } else if (PhotoViewer.Zoomer.pageIsZoomed() === true && event.keyCode === 39) {
            PhotoViewer.Scroller.scrollRight(PhotoViewer.options.columnsInRow);
        } else if (PhotoViewer.Zoomer.pageIsZoomed() === true && event.keyCode === 38) {
            PhotoViewer.Scroller.scrollUp(PhotoViewer.options.columnsInRow,
                PhotoViewer.options.album.length);
        } else if (PhotoViewer.Zoomer.pageIsZoomed() === true && event.keyCode === 40) {
            PhotoViewer.Scroller.scrollDown(PhotoViewer.options.columnsInRow,
                PhotoViewer.options.album.length);
        }
    };
};

PhotoViewer.EventManager.prototype.removeOnclickImages = function () {
    'use strict';
    var images = this.container.getElementsByTagName('img'),
        i = 0;
    for (i = 0; i < images.length; i += 1) {
        images[i].onclick = null;
    }
};

PhotoViewer.EventManager.prototype.setRightClickImage = function () {
    'use strict';
    var self = this,
        id = 0,
        builder = null;
    window.oncontextmenu = function (event) {
        id = event.explicitOriginalTarget.dataset.id;
        if(id == PhotoViewer.Scroller.getCurrentActive() && !confirm('Door de actieve te verwijderen wordt de eerste afbeelding de actieve.')) {
            return false;
        }
        builder = new PhotoViewer.PageBuilder(PhotoViewer.options.columnsInRow, PhotoViewer.options.album, PhotoViewer.options.container);
        builder.reloadWithoutImage(id);
        self.setOnclickImages();
        return false;
    };
};

PhotoViewer.EventManager.prototype.removeRightClickImage = function () {
    'use strict';
    window.oncontextmenu = null;
};

PhotoViewer.EventManager.prototype.setZoomOutClick = function () {
    'use strict';
    var self = this;
    this.container.onclick = function (event) {
        if (event.explicitOriginalTarget.className === 'column') {
            PhotoViewer.Zoomer.toggleZoom();
            self.container.onclick = null;
            self.setOnclickImages();
        }
    };
};